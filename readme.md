# ModernCV Typst Version

This is a template file port of the [modernCV](https://github.com/moderncv/moderncv) latex template file, designed to generate resumes.

## Installation

First, install [Typst](https://typst.app/).  Then download this file and link to it at the top of your resume with `import "moderncv.typ": *`.

## Usage
There are a variety of functions you can use, or mix and match to your heart's content.  They are:
1. name: Big bold name at the top of the screen
2. contact_info: place for your website, email, and github links.
3. section: section header
4. skills: double-column list of skills
5. section_details: list of details about what you did

All functions take an optional color param, if you want to change the accent color.  You can also redefine the function using `with`: `#let skills = skills.with(color: green)`
