#import "moderncv.typ": *
#name[Jeremy Bearamy]

#contact_info([example.com], [jeremy\@example.com], [gitlab.com/jbearamy])
#section[Skills]
#skills(("Time management", "Space Management", "Spacetime Management", "wibbly wobbly", "timey wimey", "bippity boppity"))

#section[Education]
#section_details([South Harmon Inst. Of Technology],
[BS in Psychology],
[South Harmon, OH],
[Graduation: May 2010])[]

#section[Experience]
#section_details([Company A], [Engineer], [Cleveland, OH], [June 2010-Present])[
- #lorem(20)
- #lorem(20)
]
