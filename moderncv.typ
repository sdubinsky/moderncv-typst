#let default_color = blue

#let update_color(col) = {
     default_color.update(col)
}
#let name(n, color: default_color) = {
     set align(center)
     text(17pt, color, n)
}
#let contact_info(website, email, github, color: default_color) = {
set align(center)
[#grid(
columns: (1fr, 1fr, 1fr),
[#text(color)[#emoji.globe.meridian] #website],
[#text(color)[#emoji.email] #email],
[#text(color)[#emoji.computer] #github])]}

#let section_details(company, title, location, time, color: default_color, details) = [
     #set list(marker: text(color)[#sym.circle.stroked.small])
    #pad(bottom: 10%,
    [#grid(
    columns: (auto, 1fr),
    align(left)[*#company* \
     #emph[#title]],
    align(right)[#location\
    #time])])
    #set list(marker: text(color)[#sym.circle.stroked.small])
     #details
    ]

#let section(title, color: default_color) = [
     #pad(top: 1em, bottom: 0.01em, [
       #set text(color, 18pt)
       #pad(bottom: -16pt, [#smallcaps[#title]])
       #pad(bottom: 0em, [#line(length: 100%, stroke: 0.3pt+color)])
])]

#let skills(stuff, color: default_color) = {
     let leftcol = ()
     let rightcol = ()
     for index, skill in stuff{
         if calc.even(index) {
            leftcol.push(skill)
         }
         
         if calc.odd(index) {
            rightcol.push(skill)
         }

}
  [#set list(marker: text(color)[#sym.circle.stroked.small])
   #grid(columns: (1fr, 1fr), [#list(..leftcol)], [#list(..rightcol)])]
  }
